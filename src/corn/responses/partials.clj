(ns corn.responses.partials
  (:require [corn.utils.router :refer [response]]
            [hiccup.page :as page]))

(defn page [{:keys [title description type]} & body]
  (response
    (page/html5
      [:head
       [:meta {:charset "utf-8"}]
       [:meta {:name "description" :content description}]
       (when (= :admin type)
         [:link {:rel "stylesheet" :href "/css/admin.css"}])
       [:title title]]
      [:body body])))
(ns corn.responses.admin.posts
  (:require [corn.utils.router :refer [redirect]]
            [corn.responses.partials :refer [page]]
            [corn.responses.partials.admin :refer [header]]))

(defn- posts [])
(defn- content []
  [:div.posts
   [:div.column.left
    [:h2.title "Ideas"]
    [:div.posts "posts go here ..."]]
   [:div.column.right
    [:h2.title "Published"]
    [:div.posts "posts go here ..."]]])

(defn response [args]
  (page {:title "Posts"
         :type :admin}
    (header)
    (content)))
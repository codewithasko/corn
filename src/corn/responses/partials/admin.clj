(ns corn.responses.partials.admin)

(defn header []
  [:div.header
   [:a {:href "/admin/editor"
        :class "plus"} "+"]
   [:a {:href "/admin/signout"} "Sign out"]])

(defn footer [])
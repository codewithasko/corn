(ns corn.core
  (:require [ring.adapter.jetty :as jetty]
            [corn.utils.router :refer [route redirect]]
            [corn.utils.session :refer [wrap-session]]
            [corn.responses :as responses]
            [corn.responses.admin.posts :as responses.admin.posts]
            [corn.data :as data])
  (:gen-class))

(defn authentication-required-middleware [request]
  (when-not (get-in request [:session :email])
    (redirect "/admin/login")))

(def routes [{:get "/"
              :do responses/home}
             {:get "/admin"
              :do responses/admin}
             {:get "/four-oh-four"
              :do responses/four-oh-four}
             {:get "/:post"
              :do responses/post}
             {:get "/admin/login"
              :do responses/admin->login}
             {:post "/admin/login"
              :do responses/admin->login!}
             {:get "/admin/signout"
              :middleware authentication-required-middleware
              :do responses/admin->signout!}
             {:get "/admin/setup"
              :do responses/admin->setup}
             {:post "/admin/setup"
              :do responses/admin->setup!}
             {:get "/admin/posts"
              :middleware authentication-required-middleware
              :do responses.admin.posts/response}])

(defn run [& args]
  (data/create-database!)
  (jetty/run-jetty (wrap-session route routes) {:port 8080})
  (prn "Server started on port 8080"))
(ns corn.responses
  (:require [corn.utils.router :refer [response redirect]]
            [hiccup.page :as page]
            [corn.data :as data]
            [crypto.password.bcrypt :as password]))

(defn- page [{:keys [title description]} & body]
  (response
    (page/html5
      [:head
       [:meta {:charset "utf-8"}]
       [:meta {:name "description" :content description}]
       [:title title]]
      [:body body])))

(defn home [_]
  (if-not (data/user-setup?)
    (redirect "/admin/setup")
    (page {:title "Hello"}
          [:h1 "Hello"])))

(defn post [args]
  (if-not (data/user-setup?)
    (redirect "/admin/setup")
    (page {:title "Post"}
          [:h1 "Post!"])))

(defn four-oh-four [_]
  (if-not (data/user-setup?)
    (redirect "/admin/setup")
    (page {:title "Four oh Four"}
          [:h1 ":("])))

(defn admin [_]
  (if (data/user-setup?)
    (redirect "/admin/login")
    (redirect "/admin/setup")))

(defn admin->login [args]
  (if-not (data/user-setup?)
    (redirect "/admin/setup")
    (page {:title "Log in"}
          [:h1 "Log in"]
          (when (get args :error)
            [:div.error (get args :error)])
          [:form {:method "post"}
           [:input {:type "email"
                    :name "email"
                    :placeholder "Your email"}]
           [:input {:type "password"
                    :name "password"
                    :placeholder "Your password"}]
           [:button {:type "submit"} "Log in"]])))


(defn admin->login! [args]
  (if-not (data/user-setup?)
    (redirect "/admin/setup")
    (let [email (get-in args [:body :email])
          password (get-in args [:body :password])]
      (if (data/user-authenticates? password)
        (-> (redirect "/admin/posts")
            (assoc :session {:email email}))
        (admin->login {:error "Oops."})))))

(defn admin->signout! [_]
  (-> (redirect "/admin/login")
      (assoc :session {})))

(defn admin->setup [args]
  (page {:title "Setup"}
        [:h1 "Setup your blog."]
        (when (get args :error)
          [:div.error (get args :error)])
        [:form {:method "post"}
         [:input {:type "text"
                  :name "name"
                  :placeholder "Blog name ..."}]
         [:input {:type "text"
                  :name "description"
                  :placeholder "Blog description ..."}]
         [:input {:type "email"
                  :name "email"
                  :placeholder "Your email"}]
         [:input {:type "password"
                  :name "password"
                  :placeholder "Your password"}]
         [:button {:type "submit"} "Set up"]]))

(defn admin->setup! [args]
  (let [name (get-in args [:body :name])
        description (get-in args [:body :description])
        email (if (empty? (get-in args [:body :email])) nil (get-in args [:body :email]))
        password (if (empty? (get-in args [:body :password])) nil (get-in args [:body :password]))]
    (if (and email password)
      (do (data/create-or-update-config! "name" name)
          (data/create-or-update-config! "description" description)
          (data/create-or-update-config! "user-email" email)
          (data/create-or-update-config! "user-password" (password/encrypt password))
          (redirect "/admin"))
      (admin->setup {:error "You need to provide your e-mail and password."}))))

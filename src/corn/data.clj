(ns corn.data
  (:require [clojure.java.jdbc :refer :all]
            [crypto.password.bcrypt :as password]
            [clojure.java.io :as io]
            [corn.utils.string :as utils.string]))

(def database {:classname "org.sqlite.JDBC"
               :subprotocol "sqlite"
               :subname "corn.db"})

(defn config [key]
  (let [config (into {} (query database ["select * from config where key = ?" key]))]
    (when-not (empty? config)
      (get config :value))))

(defn user-setup? []
  (and (config "user-email") (config "user-password")))

(defn user-authenticates? [password]
  (password/check password (config "user-password")))

(defn create-or-update-config! [key value]
  (let [config (query database ["select * from config where key = ?" key])]
    (if (empty? config)
      (insert! database :config {:key key
                                 :value value})
      (update! database :config {:value value} ["key =" key]))))

(defn create-database! []
  (when-not (.exists (io/as-file (get database :subname)))
    ;; create posts
    (try
      (db-do-commands
        database
        (create-table-ddl
          :posts
          [[:id :serial "PRIMARY KEY"]
           [:uuid :text]
           [:title :text]
           [:link :text]
           [:entry :text]
           [:published :boolean]
           [:type :text]
           [:author :text]
           [:created :int]
           [:modified :int]]))
      (catch Exception e
        (prn (.getMessage e))))

    ;; create config
    (try
      (db-do-commands
        database
        (create-table-ddl
          :config
          [[:id :serial "PRIMARY KEY"]
           [:key :text]
           [:value :text]]))
      (catch Exception e
        (prn (.getMessage e))))))
(ns corn.utils.string)

(defn uuid []
	(.toString (java.util.UUID/randomUUID)))

(defn remove-slashes-from-around [str]
  (let [str (if (clojure.string/starts-with? str "/")
              (subs str 1)
              str)
        str (if (clojure.string/ends-with? str "/")
              (subs str 0 (- (count str) 1))
              str)]
    str))
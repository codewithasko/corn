(ns corn.utils.router
  (:require [hiccup.core :as hiccup]
            [ring.util.codec :as ring.codec]
            [ring.util.response :as ring.response]
            [ring.util.request :as ring.request]
            [ring.middleware.head :as ring.head]
            [corn.utils.string :as utils.string]
            [clojure.walk :as walk]))

(defn response
  "Creates HTML page response rendered with Hiccup."
  [hiccup]
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body (hiccup/html hiccup)})

(defn response->404 []
  (response [:div "Page not found."]))

(defn redirect
  "Creates a redirect response."
  [to]
  {:status 302
   :headers {"Location" to}
   :body nil})

(defn route-path-matches?
  "Replaces any occurrence in `path` that matches a keyword with a `w+`
  to match any word, then it tries to match it against `request-path`
  to see if we have a route path that we want.

  Returns `true` or `nil`."
  [path request-path]
  (let [path (utils.string/remove-slashes-from-around path)
        request-path (utils.string/remove-slashes-from-around request-path)
        regex (clojure.string/replace path #":(\w+)" "\\\\w+")]
    (when (re-matches (re-pattern regex) request-path)
      true)))

(defn route-path-params
  "Composes a map of `:key value` parameters from the keywords specified in `path`
  corresponding the actual result from `request-path`.

  Returns a sorted map."
  [path request-path]
  (let [split-path (vec (remove #(= "" %) (clojure.string/split path #"/")))
        split-request-path (vec (remove #(= "" %) (clojure.string/split request-path #"/")))]
    (into (sorted-map) (map-indexed (fn [index item]
                                      (when (clojure.string/starts-with? item ":")
                                        {(keyword (subs item 1)) (get split-request-path index)}))
                                    split-path))))

(defn- compose-responses [request routes]
  (let [request-path (get request :uri)
        request-method (get request :request-method)]
    (for [route routes]
      (let [method (key (first route))
            path (val (first route))
            middleware  (if (get route :middleware)
                          (get route :middleware)
                          (fn [_] nil))
            _ (prn middleware)
            response (get route :do)]
        (when (and (= method request-method)
                   (route-path-matches? path request-path))
          (if (middleware request)
            (middleware request)
            (response {:body (when (get request :body)
                               (walk/keywordize-keys (ring.codec/form-decode (slurp (get request :body)))))
                       :query-string (get request :query-string)
                       :params (route-path-params path request-path)
                       :session (get request :session)})))))))

(defn- compose-resource [request root-path]
  (when (#{:head :get} (:request-method request))
    (let [path (subs (ring.codec/url-decode (ring.request/path-info request)) 1)]
      (-> (ring.response/resource-response path {:root root-path})
          (ring.head/head-response request)))))

(defn route
  "Attempts to match the given `request` with the given `routes`,
  and upon successful match calls the `callback` with a map as its only parameter:

  ```
  {:body ...
   :query-string ...
   :params ...
  }
  ```
  "
  [request routes]
  (let [responses (compose-responses request routes)
        response (first (remove nil? responses))
        resource (compose-resource request "public")]
    (cond
      resource resource
      response response
      :else (response->404))))
(ns corn.utils.session
  (:require [ring.middleware.session :as session]
            [ring.middleware.session.cookie :as cookie]))

(defn- session-options
  [options]
  {:store        (options :store (cookie/cookie-store))
   :cookie-name  (options :cookie-name "ring-session")
   :cookie-attrs (merge {:path      "/"
                         :http-only true}
                        (options :cookie-attrs)
                        (if-let [root (options :root)]
                          {:path root}))})

(defn wrap-session
  "Reads in the current HTTP session map, and adds it to the :session key on
  the request. If a :session key is added to the response by the handler, the
  session is updated with the new value. If the value is nil, the session is
  deleted.

  Accepts the following options:

  :store        - An implementation of the SessionStore protocol in the
                  ring.middleware.session.store namespace. This determines how
                  the session is stored. Defaults to in-memory storage using
                  ring.middleware.session.store/memory-store.

  :root         - The root path of the session. Any path above this will not be
                  able to see this session. Equivalent to setting the cookie's
                  path attribute. Defaults to \"/\".

  :cookie-name  - The name of the cookie that holds the session key. Defaults to
                  \"ring-session\".

  :cookie-attrs - A map of attributes to associate with the session cookie.
                  Defaults to {:http-only true}. This may be overridden on a
                  per-response basis by adding :session-cookie-attrs to the
                  response."
  ([handler routes]
   (wrap-session handler routes {}))
  ([handler routes options]
   (let [options (session-options options)]
     (fn
       ([request]
        (let [request (session/session-request request options)]
          (-> (handler request routes)
              (session/session-response request options))))
       ([request respond raise]
        (let [request (session/session-request request options)]
          (handler request
                   routes
                   (fn [response]
                     (respond (session/session-response response request options)))
                   raise)))))))

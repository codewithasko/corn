(ns corn.utils.router_test
  (:require [clojure.test :refer :all]
            [corn.utils.router :as router]
            [hiccup.core :as hiccup]))

(deftest response-test
  (testing "A regular response"
    (is (= {:status  200
            :headers {"Content-Type" "text/html"}
            :body    (hiccup/html [:div "hello"])}
           (router/response [:div "hello"])))))

(deftest redirect-test
  (testing "A regular redirect"
    (is (= {:status 302
            :headers {"Location" "https://google.com"}
            :body nil}
           (router/redirect "https://google.com")))))

(deftest route-path-matches?-test
  (testing "A regular path that matches"
    (is (= true (router/route-path-matches? "/hello/some/page" "/hello/some/page"))))
  (testing "A regular path that does not match"
    (is (= nil (router/route-path-matches? "/hello/some/page" "/hello/some/other/page"))))
  (testing "A regular path with no leading slash that matches"
    (is (= true (router/route-path-matches? "hello/some/page" "/hello/some/page"))))
  (testing "A regular page with no leading slash that does not match"
    (is (= nil (router/route-path-matches? "hello/some/page" "/hello/some/other/page"))))
  (testing "A path with keywords that matches"
    (is (= true (router/route-path-matches? "/hello/:name" "/hello/john")))
    (is (= true (router/route-path-matches? "/hello/:name/:and/:other", "/hello/john/but/neither"))))
  (testing "A path with keywords that does not match"
    (is (= nil (router/route-path-matches? "/hello/:name" "/hello")))
    (is (= nil (router/route-path-matches? "/hello/:name" "/hi")))))

(deftest route-path-params-test
  (testing "Getting parameters back correctly"
    (is (= {:a "b"
            :b "c"
            :c "d"}
           (router/route-path-params "/:a/:b/:c" "/b/c/d"))))
  (testing "Getting no parameters back"
    (is (= {}
           (router/route-path-params "/a/b/c" "/a/b/c")))))

(deftest route-test
  (let [routes [{:get "/" :do (fn [args] {:hello :world})}
                {:get "/:name" :do (fn [args] {:hello (get-in args [:params :name])})}]]
    (testing "Getting a route back"
      (is (= {:hello :world} (router/route {:uri "/"
                                            :request-method :get
                                            :query-string nil
                                            :body nil
                                            :session nil} routes)))
      (is (= {:hello "john"} (router/route {:uri "/john"
                                            :request-method :get
                                            :query-string nil
                                            :body nil
                                            :session nil} routes))))
    (testing "Not getting a route back"
      (is (= {} (router/route {:uri "/hello/world"
                               :request-method :get
                               :query-string nil
                               :body nil
                               :session nil} routes))))))